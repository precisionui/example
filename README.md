# PrecisionUI Example
## Building
Replace `pacman` with `dkp-pacman` if you installed `dkp-pacman` manually (don't do this on Windows MSYS!)
* Install devkitPro (with switch-dev enabled)
* Run `git clone https://gitlab.com/precisionui/example.git`
* Run `cd example`
* Run `git submodule update --init --recursive`
* Run `sudo dkp-pacman -Sy switch-sdl2 switch-sdl2_gfx switch-sdl2_image switch-sdl2_ttf`
* Run `make`
* Upload the file at dist/example.nro to your switch